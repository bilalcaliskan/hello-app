# Hello App

This app is written with Golang 1.15 to simulate simple Hello world message over a REST API. This app uses `github.com/gorilla/mux` to 
spin up a HTTP router on port 11130. Also spins up another HTTP router with `net/http` to expose application metrics on port 2112 with below libraries;

	github.com/prometheus/client_golang v1.7.1
	github.com/prometheus/common v0.14.0 // indirect
	github.com/prometheus/procfs v0.2.0 // indirect

I also introduced a custom metric myapp_processed_ops_total which counts the requests hit the endpoint / and exposes over /metrics endpoint. Exposed metrics will be 
fetched by Prometheus to used by `KEDA`(<https://github.com/kedacore/keda>) for event driven autoscaling.

## Customizable Environment Variables

The following list introduces the customizable environment variables with default values;

```ShellSession
SERVER_PORT=11130
WRITE_TIMEOUT_SECONDS=10
READ_TIMEOUT_SECONDS=10
```

## Continuous Integration

I decided to use Gitlab CI for Continuous Integration so code is on Gitlab instead of Github. Pipeline is triggered when a new commit 
pushed to master branch. As you see in the `.gitlab-ci.yml`, a bash script on `ci-scripts/release.sh` executed when pipeline run. That 
script builds the Docker image, push to the <https://hub.docker.com/r/bilalcaliskan/hello-app/tags>, automatically increases the versions in the `version.properties`, `charts/hello-app/Chart.yaml`, `charts/hello-app/values.yaml`, tags 
and commits to the master branch with `[ci-skip]` not to re-trigger CI job on Gitlab CI. 

Version in the `version.properties` is for 
tracking the latest released version, but the versions in the  `charts/hello-app/Chart.yaml`, `charts/hello-app/values.yaml` are for Continuous Deployment, i will explain the details on the next section. 

On the Docker build; i decided to use multi stage Docker build to minimize the final container image. In the first stage, i have used the `golang:1.15-alpine` as base image. 
In the final stage, base image is `alpine:latest`.

## Continuous Deployment

I decided to use `Fluxcd Helm Operator`(<https://github.com/fluxcd/helm-operator>) for Continuous Deployment. helm-operator is deployed on a namespace flux in the K8s environment. It periodically checks a repo 
on version control system and applies to the cluster using Helm. So i have `charts` folder for that purpose. When new commit arrives to the master branch, Docker image is built and pushed with Gitlab CI, 
versions in the `charts/hello-app/Chart.yaml`, `charts/hello-app/values.yaml` files updated with the latest image version. So after that moment helm-operator checks the repo and see the changes in the version files, applies the changes to the cluster.

Here is the diagram of the Continuous Deployment flow;

![alt text](https://docs.fluxcd.io/projects/helm-operator/en/stable/_files/fluxcd-helm-operator-diagram.png)