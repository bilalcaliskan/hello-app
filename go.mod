module bilalcaliskan/hello-app

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/prometheus/client_golang v1.7.1
	github.com/prometheus/common v0.14.0 // indirect
	github.com/prometheus/procfs v0.2.0 // indirect
	golang.org/x/sys v0.0.0-20200930185726-fdedc70b468f // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)
