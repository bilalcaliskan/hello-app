package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"log"
	"net/http"
	"time"
)

var (
	opsProcessed = promauto.NewCounter(prometheus.CounterOpts{
		Name: "myapp_processed_ops_total",
		Help: "The total number of processed events",
	})
)

func main() {
	serverPort := getIntEnv("SERVER_PORT", 11130)
	writeTimeoutSeconds := getIntEnv("WRITE_TIMEOUT_SECONDS", 10)
	readTimeoutSeconds := getIntEnv("READ_TIMEOUT_SECONDS", 10)
	router := mux.NewRouter()

	go func() {
		http.Handle("/metrics", promhttp.Handler())
		log.Println("Prometheus exporter is listening on port 2112!")
		log.Fatal(http.ListenAndServe(":2112", nil))
	}()

	server := initServer(router, fmt.Sprintf(":%d", serverPort), time.Duration(int32(writeTimeoutSeconds)) * time.Second,
		time.Duration(int32(readTimeoutSeconds)) * time.Second)
	log.Printf("Server is listening on port %d!", serverPort)
	log.Fatal(server.ListenAndServe())
}